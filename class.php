<?php

/* Hoofd-Class */
	 abstract class mathOperations{

	protected $firstValue = null;
	protected $secondValue = null;
	protected $result = null;

   /* constructor method */
	public function __construct(){
    }

	 // Get eerste waarde
	public function getFirstValue(){
			return $this->firstValue;
		}

		// get tweede waarde
		public function getSecondValue(){
		   return $this->secondValue;	
		}

		// public method bereken waarde
		abstract public function calculation();
			
	}


	// Class optellen
	class Sum extends mathOperations {

		// Constructor
		public function __construct($firstValue, $secondValue){
			$this->firstValue = $firstValue;
			$this->secondValue = $secondValue;
		}

		// public method bereken waarde
		public function calculation(){
			$this->result = $this->firstValue + $this->secondValue;
			return $this->result;
		}

	}	


	// Class aftrekken
	class subtract extends mathOperations {

		// constructor
		public function __construct($firstValue, $secondValue){
			$this->firstValue = $firstValue;
			$this->secondValue = $secondValue;
		}

		// public method bereken waarde
		public function calculation(){
			$this->result = $this->firstValue - $this->secondValue;
			return $this->result;
		}

	}


	// class multiply
	class multiply extends mathOperations {

		//constructor
		public function __construct($firstValue, $secondValue){
			$this->firstValue = $firstValue;
			$this->secondValue = $secondValue;
		}

		// public method bereken waarde
		public function calculation(){
		 		$this->result = $this->firstValue * $this->secondValue;
			return $this->result;
		}
	}
?>